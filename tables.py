__author__ = 'Brian Snyder'

import database
from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship, mapper
from sqlalchemy import Table

domain = Table('domains', database.metadata,
               Column('id', Integer, primary_key=True),
               autoload=True, autoload_with=database.engine)

records = Table('records', database.metadata,
                Column('id', Integer, primary_key=True),
                Column('domain_id', Integer, ForeignKey('domains.id')),
                autoload=True, autoload_with=database.engine)


class Domains():
    pass


class Records():
    pass

mapper(Domains, domain)
mapper(Records, records)

database.Base.metadata.create_all(database.engine)
Session = sessionmaker(bind=database.engine)
