__author__ = 'Brian Snyder'

import sys
from PySide.QtCore import *
from PySide.QtGui import *
from tables import Domains, Records, Session
from ui_zonetable import Ui_Dialog
from models import CustomModel


class PowerdnsGui(QWidget, Ui_Dialog):
    def __init__(self, parent=None):
        super(PowerdnsGui, self).__init__(parent)
        self.setupUi(self)
        self.session = Session()
        self.model = CustomModel(self.get_domain_list(self.session),['name'])
        self.proxymodel = QSortFilterProxyModel()
        self.proxymodel.setSourceModel(self.model)
        self.proxymodel.setFilterKeyColumn(0)
        self.zonestbl.setModel(self.proxymodel)
        self.zonestbl.resizeColumnsToContents()
        self.zonetxt.textChanged.connect(self.proxymodel.setFilterWildcard)

    def get_domain_list(self, session):
        """Return query of all domains"""
        return session.query(Records.name).filter_by(type='SOA').all()

    def get_records(self, domain_id, session):
        """Return query of all rr records for selected domain_id"""
        return session.query(records).filter_by(domain_id=domain_id)

    def only_SOA_record(self, record_query, session):
        return record_query.filter_by(type='SOA')

    def save_record(self, session):
        """commit any changes made to the records.  return commit status"""

    def delete_record(self, id, session):
        """delete selected id from records table.  return delete status"""

    def delete_domain(self, id, session):
        """delete selected id from domains table.   return delete status"""

    def add_domain(self, session, domain):
        """add domain to domains table"""

    def add_record(self, session, record):
        """add rr record to the records table"""



def main():
    """Main method for PowerDNS Gui"""
    app = QApplication(sys.argv)
    mainwindow = PowerdnsGui()
    mainwindow.show()
    app.exec_()

if __name__ == '__main__':
    sys.exit(main())