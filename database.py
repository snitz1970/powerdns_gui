__author__ = 'Brian Snyder'

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
import configparser

config = configparser.ConfigParser()
config.read('database.cfg')
host = config['database']['host']
username = config['database']['username']
password = config['database']['password']
db = config['database']['db']

engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(
    username, password, host, db), echo=True)

Base = declarative_base()
metadata = Base.metadata










