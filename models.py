from PySide import QtCore


class CustomModel(QtCore.QAbstractTableModel):
    def __init__(self, data = [], fieldList = []):   ## send param for query in future
        super(CustomModel, self).__init__()     ###add option for columns to display
        self._data = data

        self.fieldList = fieldList
#        self.tablename = tablename



    def setData(self, index, value, role=QtCore.Qt.EditRole):
        #TODO: add code to update fields, not just add new ones.  If a record
        #changes query data first, then add if does not exist
        row = index.row()
        column = index.column()

        setattr(self._data[row], self.fieldList[column], value)
        self.dataChanged.emit(index, index)
        return True

    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
#        return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def rowCount(self, index=QtCore.QModelIndex()):
        return len(self._data)

    def columnCount(self, index=QtCore.QModelIndex()):
        return len(self.fieldList)

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.TextAlignmentRole:
            if orientation == QtCore.Qt.Horizontal:
                return int(QtCore.Qt.AlignLeft)
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return self.fieldList[section].upper()

    def loadData(self, data):
        self.beginResetModel()
        self._data = data
        self.endResetModel()

    def data(self, index, role):
        row = index.row()
        column = index.column()
#        print(row, column)
        #TODO: find out if data is a list of data, or objects.
        if role == QtCore.Qt.DisplayRole:
            return getattr(self._data[row],self.fieldList[column])
        if role == QtCore.Qt.EditRole:              ######
                                                    ######
                                                    ##EditRole is required if
                                                    ##using QDataWidgetMapper
            return getattr(self._data[row],self.fieldList[column])


    def insertRows(self, position, rows=1, index=QtCore.QModelIndex()):
        self.beginInsertRows(QtCore.QModelIndex(), position,
                            position + rows -1)
        for row in range(rows):
            self._data.insert(position + row, self.tablename())
        self.endInsertRows()
        return True

