# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Brian Snyder\Dropbox\Python projects\DNS Utils\DNS utils\zonetable.ui'
#
# Created: Wed Jul  2 11:56:02 2014
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(306, 269)
        self.gridLayout = QtGui.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtGui.QLabel(Dialog)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.zonetxt = QtGui.QLineEdit(Dialog)
        self.zonetxt.setObjectName("zonetxt")
        self.horizontalLayout_2.addWidget(self.zonetxt)
        self.gridLayout.addLayout(self.horizontalLayout_2, 0, 0, 1, 1)
        self.zonestbl = QtGui.QTableView(Dialog)
        self.zonestbl.setObjectName("zonestbl")
        self.gridLayout.addWidget(self.zonestbl, 1, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.addzoneButton = QtGui.QPushButton(Dialog)
        self.addzoneButton.setObjectName("addzoneButton")
        self.horizontalLayout.addWidget(self.addzoneButton)
        self.deletezoneButton = QtGui.QPushButton(Dialog)
        self.deletezoneButton.setObjectName("deletezoneButton")
        self.horizontalLayout.addWidget(self.deletezoneButton)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.pushButton_3 = QtGui.QPushButton(Dialog)
        self.pushButton_3.setObjectName("pushButton_3")
        self.horizontalLayout.addWidget(self.pushButton_3)
        self.gridLayout.addLayout(self.horizontalLayout, 2, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.pushButton_3, QtCore.SIGNAL("clicked()"), Dialog.close)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "DNS", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Dialog", "Look for Zone", None, QtGui.QApplication.UnicodeUTF8))
        self.addzoneButton.setText(QtGui.QApplication.translate("Dialog", "Add New Zone", None, QtGui.QApplication.UnicodeUTF8))
        self.deletezoneButton.setText(QtGui.QApplication.translate("Dialog", "Delete Zone", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_3.setText(QtGui.QApplication.translate("Dialog", "Quit", None, QtGui.QApplication.UnicodeUTF8))

